module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jsbeautifier: {
            files: ['src/**/*.html'],
            options: {}
        },
        accessibility: {
            options: {
                accessibilityLevel: 'WCAG2A'
            },
            test: {
                src: ['src/**/*.html']
            }
        },
        'http-server': {
            'dev': {
                root: 'src/',
                port: 8282,
                host: "0.0.0.0",
                ext: "html",
                runInBackground: false
            }
        },
        'sftp-deploy': {
            build: {
                auth: {
                    host: 'frontend.ardev.us',
                    authKey: 'privateKey'
                },
                cache: 'sftpCache.json',
                src: 'src/',
                dest: '/www/development/<%= pkg.name %>',
                exclusions: ['build/', 'node_module/', 'Gruntfile.js', 'package.json', 'readme.md', '.sass-cache', '.git', '.gitignore'],
                serverSep: '/',
                concurrency: 4,
                progress: true
            }
        },
        replace: {
            cdn: {
				src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
	            overwrite: true,
	            replacements: [{
	                from: 'http://localhost:8282/to_origin/',
	                to: '/e2/'
	            }, {
	                from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
	                to: '/e2/'
	            }, {
	                from: 'http://frontend.ardev.us/api/',
	                to: 'http://www.army.mil/api/'
	            }]
            },
            dev: {
				src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
	            overwrite: true,
	            replacements: [{
	                from: /\/e2\/(?!rv5_js\/3rdparty|rv5_js\/main|rv5_js\/features|rv5_css\/features|rv5_images\/features)/g,
	                to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
	            }, {
	                from: 'http://localhost:8282/to_origin/',
	                to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
	            }, {
	                from: 'http://www.army.mil/api/',
	                to: 'http://frontend.ardev.us/api/'
	            }]
            }
        }
    });


    grunt.loadNpmTasks('grunt-sftp-deploy');

    grunt.loadNpmTasks('grunt-http-server');

    grunt.loadNpmTasks('grunt-accessibility');

    grunt.loadNpmTasks("grunt-jsbeautifier");

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.registerTask('dev', ['replace:dev']);

    grunt.registerTask('cdn', ['replace:cdn']);

};
